PayUnit PHP SDK
=======================

This is a payment sdk developed by the SevenGPS for it's Payunit agreggator platform


Requirements
============

* PHP >= 7.2 (not working with PHP 7.3)
* Latest cURL Extension


Installation
============

        composer require sevengps/payunit


Usage
=====

Add the PayUnit Namespace in your desired controller 

        use SevenGps\PayUnit


Create a new instance of the PayUnit class and pass in all the required attributes



  - ``` 
      $myPayment = new PayUnit(
        "your_api_key",
        "your_api_password",
        "your_api_username",
        "returnUrl",
        "notifyUrl",
        "mode",
        "description",
        "purchaseRef",
        "currency",
        "merchant_name",
        "transaction_id (This id should be alpha numeric and less than 20 characters)"
    );  
    ```

          
The Mode can be ```live``` or  ```test```



Call the MakePayment method. This method will redirect to the payment page where the client can perform payment with the payment method you have activated on your dashboard

        $myPayment->makePayment("amount");

Recommendations
=======

Please for security reasons make sure you read your Api key, Api password and Api user are set in an environment config file

Demo

link to demo application https://gitlab.com/sevencommonfactor/payunit-php-demo/-/tree/dev

## License

The Payunit PHP SDK is released under a Propretary license [SevenGPS](https://www.sevengps.net).
