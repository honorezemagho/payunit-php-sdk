<?php

namespace SevenGps;

class PayUnit
{
  public string $apiKey;
  public string $apiPassword;
  public string $apiUser;
  public string $returnUrl;
  public string $notifyUrl;
  public string $mode;
  public string $description;
  public string $purchaseRef;
  public string $currency;
  public string $name;
  public string $transactionId;

  /**
   * PayUnit constructor.
   * @param string $apiKey Your apikey
   * @param string $apiPassword Your apiPassword
   * @param string $apiUser Your apiUsername
   * @param string $returnUrl Your return Url
   * @param string $notifyUrl Your notify Url
   * @param string $mode Transaction mode
   * @param string $description description of the transaction
   * @param string $purchaseRef purchaseRef
   * @param string $currency currency
   * @param string $name Merchant Name
   * @param string $transactionId Merchant transaction id
   */
  public function __construct(
    string $apiKey,
    string $apiPassword,
    string $apiUser,
    string $returnUrl,
    string $notifyUrl,
    string $mode,
    string $description,
    string $purchaseRef,
    string $currency,
    string $name,
    string $transactionId
  ) {
    $this->apiKey = $apiKey;
    $this->apiPassword = $apiPassword;
    $this->apiUser = $apiUser;
    $this->returnUrl = $returnUrl;
    $this->notifyUrl = $notifyUrl;
    $this->mode = $mode;
    $this->description = $description;
    $this->purchaseRef = $purchaseRef;
    $this->currency = $currency;
    $this->name = $name;
    $this->transactionId = $transactionId;
  }

  /**
   * Used to perform the Transaction
   * @param float $amountTobePaid Amount to be paid
   * @throws Exception When invalid parameters are passed
   * @throws Exception When transaction fails due to server error.
   */
  public function makePayment(float $amountTobePaid): void
  {

    try {

      $encodedAuth = base64_encode($this->apiUser . ":" . $this->apiPassword);
      $postRequest = array(
        "total_amount" => $amountTobePaid,
        "return_url" => $this->returnUrl,
        "notify_url" => $this->notifyUrl,
        "currency" => $this->currency,
        "purchaseRef" => $this->purchaseRef,
        "name" => $this->name,
        "description" => $this->description,
        "transaction_id" => $this->transactionId
      );

      $cURLConnection = curl_init();
      curl_setopt($cURLConnection, CURLOPT_URL, "https://app.payunit.net/api/gateway/initialize");
      curl_setopt($cURLConnection, CURLOPT_POSTFIELDS, json_encode($postRequest));
      $secArr =  array(
        "x-api-key: {$this->apiKey}",
        "authorization: Basic: {$encodedAuth}",
        'Accept: application/json',
        'Content-Type: application/json',
        "mode: {$this->mode}"
      );

      curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, $secArr);
      curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
      $apiResponse = curl_exec($cURLConnection);
      curl_close($cURLConnection);


      if ($apiResponse === false) {
        throw new \Exception("Failed to connect to PayUnit API");
      }


      $jsonArrayResponse = json_decode($apiResponse, true);

      if ($jsonArrayResponse->status == "SUCCESS" && $jsonArrayResponse->statusCode == 200) {

        header("Location: {$jsonArrayResponse->data->transaction_url}");
        exit();
      }

      $messages = $jsonArrayResponse->message;

      if (is_array($messages)) {
        $messages = implode(", ", $messages);
      }

      if ($jsonArrayResponse->statusCode == 400) {
        throw new  \Exception('Please check the parameters passed, ' . $messages);
      }
      if ($jsonArrayResponse->statusCode == 500) {
        throw new  \Exception('An error occured, this could be an issue on our side, please try again later');
      }
    } catch (\Exception $e) {
      throw new \Exception($e->getMessage(), $e->getCode());
    }
  }
}
